# Presentacio

Utilitzaré aquest mk com a presentació i portfolio per els meus projectes.

## Aplicació Spring boot

![1_-uckV8DOh3l0bCvqZ73zYg](https://gitlab.com/mrieramar/projecte1SpringBoot/uploads/ceb3a927584a9389b570d9ae82f245eb/1_-uckV8DOh3l0bCvqZ73zYg.webp)

Aplicació basada en Spring Boot, que he fet seguin un curs de desenvolupament. Les tenconologies utilitzades seràn:

* Spring Boot
* Mysql
* Ajax
* HTML
* Bootstrap

[Enllaç al projecte](https://gitlab.com/mrieramar/projecte1SpringBoot)
